<?php

namespace App\Form;

use App\Entity\FicheFrais;
use App\Entity\Etat;
use App\Form\LigneFraisForfaitType;
use App\Form\LigneFraisHorsForfaitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class FicheFraisType extends AbstractType
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mois', DateType::class, [
                'widget' => 'single_text',
                'disabled' => true,
                'format' => 'MMMM',
                'html5' => false
              ]);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
          $form = $event->getForm();
          if ($this->session->get('role') === "Visiteur") {
            $form->add('fk_vehicule');
          } else {
            $form->add('fk_etat', EntityType::class, [
              'class' => Etat::class,
              'choice_label' => 'libelle'
            ]);
            $form->add('fk_vehicule', null, ['disabled' => true]);
          }
        });
        $builder->add('ligneFraisForfaits', CollectionType::class, [
                'entry_type' => LigneFraisForfaitType::class,
                'entry_options' => ['label' => false],
                'label' => false
              ])
            ->add('ligneFraisHorsForfaits', CollectionType::class, [
                'entry_type' => LigneFraisHorsForfaitType::class,
                'entry_options' => ['label' => false],
                'label' => false,
                'allow_add' => true,
                'allow_delete' => true
              ])
            ->add('save', SubmitType::class, [
                'attr' => ['class' => 'btn btn-secondary'],
              ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FicheFrais::class,
        ]);
    }
}
