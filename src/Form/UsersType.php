<?php

namespace App\Form;

use App\Entity\Users;
use App\Entity\Role;
use App\Entity\MaSession;
use App\Form\RegistrationFormType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TelType;

class UsersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('registration', RegistrationFormType::class,[
              "mapped" => false
            ])
            ->add('nom')
            ->add('prenom')
            ->add('dateNaissance', DateType::class, [
                'years' => range(1945,2003),
                'format' => 'dd MM yyyy'
            ])
            ->add('codePostal')
            ->add('ville')
            ->add('adresse')
            ->add('email', EmailType::class)
            ->add('telephone', TelType::class)
            ->add('dateEmbauche', DateType::class, [
                'years' => range(1945,2045),
                'format' => 'dd MM yyyy'
            ])
            ->add('matricule')
            ->add('role', null, [
				'class' => Role::class,
				'choice_label' => 'libelle', ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Users::class,
        ]);
    }
}
