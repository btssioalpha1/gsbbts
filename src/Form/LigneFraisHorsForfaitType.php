<?php

namespace App\Form;

use App\Entity\LigneFraisHorsForfait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\File;

class LigneFraisHorsForfaitType extends AbstractType
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
          // Condition pour le champ 'quantiter', il ne peut être modifiable que par les visiteurs
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
          $form = $event->getForm();
          if ($this->session->get('role') === "Visiteur") {
            $form->add('libelle')
                 ->add('date', DateType::class, ['widget' => 'single_text'])
                 ->add('montant', NumberType::class, ['attr' => ['type' => 'number'],])
                 ->add('justificatif', FileType::class, [
                         'label' => 'Justificatif',
                         'mapped' => false,
                         'required' => false,
                         'constraints' => [
                             new File([
                                 'maxSize' => '2048k',
                                 'mimeTypes' => [
                                     'image/jpeg',
                                     'image/png',
                                 ],
                                 'mimeTypesMessage' => 'Veuillez choisir un jpeg ou png.',
                             ])
                         ],
                     ])
                ->add('validee', null, ['disabled' => true, 'label' => false, 'attr' => ['hidden' => true]])
            ;
          } else {
            $form->add('libelle', null, ['disabled' => true])
                 ->add('date', DateType::class, ['widget' => 'single_text','disabled' => true])
                 ->add('montant', NumberType::class, ['disabled' => true, 'attr' => ['type' => 'number']])
                 ->add('validee', null, ['disabled' => false])
                 ->add('bypass', null, ['label' => false, 'mapped' => false, 'attr' => ['hidden' => true]])
            ;
          }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LigneFraisHorsForfait::class,
        ]);
    }
}
