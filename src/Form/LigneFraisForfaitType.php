<?php

namespace App\Form;

use App\Entity\LigneFraisForfait;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class LigneFraisForfaitType extends AbstractType
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
          // Condition pour le champ 'quantiter', il ne peut être modifiable que par les visiteurs
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
          $form = $event->getForm();
          if ($this->session->get('role') === "Visiteur") {
            $form->add('quantite', null, [
              "label" => false,
              "required" => true,
              "disabled" => false
            ]);
          } else {
            $form->add('quantite', null, [
              "label" => false,
              "required" => true,
              "disabled" => true
            ]);
          }
        });
          // Condition pour le champ 'valider', il ne peut être visible et accessible que par les comptables
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
          if ($this->session->get('role') === "Comptable") {
            $form = $event->getForm();
            $form->add('etatValidationLigne', CheckboxType::class, [
              "label" => false,
              "required" => false
            ]);
          }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LigneFraisForfait::class,
        ]);
    }
}
