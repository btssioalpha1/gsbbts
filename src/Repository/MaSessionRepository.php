<?php

namespace App\Repository;

use App\Entity\MaSession;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MaSession|null find($id, $lockMode = null, $lockVersion = null)
 * @method MaSession|null findOneBy(array $criteria, array $orderBy = null)
 * @method MaSession[]    findAll()
 * @method MaSession[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MaSessionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MaSession::class);
    }

    // /**
    //  * @return MaSession[] Returns an array of MaSession objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MaSession
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
