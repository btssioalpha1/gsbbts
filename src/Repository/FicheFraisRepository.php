<?php

namespace App\Repository;

use App\Entity\FicheFrais;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method FicheFrais|null find($id, $lockMode = null, $lockVersion = null)
 * @method FicheFrais|null findOneBy(array $criteria, array $orderBy = null)
 * @method FicheFrais[]    findAll()
 * @method FicheFrais[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FicheFraisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FicheFrais::class);
    }

    // /**
    //  * @return FicheFrais[] Returns an array of FicheFrais objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FicheFrais
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAll() {
      return $this->findBy(array(), array('mois' => 'DESC'));
    }

    public function findLast() {
      $tmp = $this->findBy(array(), array('mois' => 'DESC'));
      return $tmp ? $tmp[0] : null;
    }

    public function existThisCurrentMonth() {
      $conn = $this->getEntityManager()->getConnection();
      $sql = '
        SELECT *
        FROM fiche_frais
        WHERE Year(mois) = Year(CURRENT_TIMESTAMP)
        AND Month(mois) = Month(CURRENT_TIMESTAMP)
      ';
      $stmt = $conn->prepare($sql);
      $stmt->execute();
      return $stmt->fetchAll();
    }

    public function statFFparMois() {
      $conn = $this->getEntityManager()->getConnection();
      $sqlStats = '
        SELECT YEAR(mois) as Annee, MONTHNAME(mois) as Mois, SUM(montant_total) as Montant_total, COUNT(*) as Nombre_de_fiche_enregistré, AVG(montant_total) as moyenneMonth
        FROM fiche_frais
        GROUP BY YEAR(mois), MONTH(mois)
      ';
      $stmt = $conn->prepare($sqlStats);
      $stmt->execute();
      return $stmt->fetchAll();
    }
}
