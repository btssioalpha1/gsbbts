<?php

namespace App\Controller;

use App\Entity\FicheFrais;
use App\Entity\LigneFraisForfait;
use App\Entity\LigneFraisHorsForfait;
use App\Entity\FraisForfait;
use App\Entity\Users;
use App\Entity\Etat;
use App\Entity\Justificatif;
use App\Form\FicheFraisType;
use App\Repository\FicheFraisRepository;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @Route("/fichefrais")
 */
class FicheFraisController extends AbstractController
{

    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    /**
     * @Route("/", name="fiche_frais_index", methods={"GET"})
     */
    public function index(FicheFraisRepository $ficheFraisRepository): Response
    {
          // Si on est comptable on récupère toutes les fiches frais
        if($this->session->get('role') === "Comptable") {
          $fichefrais = $ficheFraisRepository->findAll();
          // Si on est visiteur on récupère les fiches liés à l'utilisateur et disponible en saisie
        } else {
          $fichefrais = $ficheFraisRepository->findBy([
            'fk_users' => $this->session->get('id'),
            'fk_Etat' => $this->getDoctrine()->getRepository(Etat::class)->findOneBy(['libelle' => 'Saisie'])
          ]);
        }
        return $this->render('fiche_frais/index.html.twig', [
            'fiche_frais' => $fichefrais,
        ]);
    }

    /**
     * @Route("/new", name="fiche_frais_new", methods={"GET","POST"})
     */
    public function new(Request $request, FicheFraisRepository $ficheFraisRepository): Response
    {
        if($this->session->get('role') === "Visiteur") { // Seul les visiteurs peuvent créer des fiches de frais
          $ficheFrai = new FicheFrais();
          /* Redirige sur l'edit si FF déjà existante */
          if($ficheFraisRepository->existThisCurrentMonth() && false) {
            $ficheFrai = $ficheFraisRepository->findLast();
            return $this->redirectToRoute('fiche_frais_edit', ['id' => $ficheFrai->getId()]);
          }

          /* Sinon on traite */
          $ficheFrai->setMois(new \DateTime());
          $ficheFrai->setMontantValide(0);
          $ficheFrai->setFkEtat($this->getDoctrine()->getRepository(Etat::class)->findOneBy(['libelle' => 'Saisie']));

          // Pour chaque Frais Forfait qu'on trouve en bdd, on ajoute une ligne frais forfait dans l'objet fiche de frais
          $arrayFraisForfait = $this->getDoctrine()->getRepository(FraisForfait::class)->findAll();
          foreach ($arrayFraisForfait as $oneFraisForfait) {
            $ligneFraisForfait = new LigneFraisForfait();
            $ligneFraisForfait->setDateMois($ficheFrai->getMois());
            $ligneFraisForfait->setFkFraisForfait($oneFraisForfait);
            $ligneFraisForfait->setEtatValidationLigne(false);
            $ficheFrai->getLigneFraisForfaits()->add($ligneFraisForfait);
          }

            // La création du form fiche de frais entrainera celui des lignes frais forfait et hors forfait
            // (voir FicheFraisType.php)
          $formFiche = $this->createForm(FicheFraisType::class, $ficheFrai);
            // Traitement lors du POST
          $formFiche->handleRequest($request);
          if ($formFiche->isSubmitted() && $formFiche->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
                      // ----- Fiche Frais ----- //
            $ficheFrai->setFkUsers($this->getDoctrine()->getRepository(Users::class)->find($this->session->get('id')));
            $ficheFrai->setDateModification(new \DateTime());
            $entityManager->persist($ficheFrai);
            $totalFF = 0;
            $nbJustificatif = 0;
            $i = 0;
                      // ----- Ligne Frais Forfait ----- //
            foreach ($ficheFrai->getLigneFraisForfaits() as $uneLigne) {
              $uneLigne->setFkFicheFrais($ficheFrai);
              $totalFF += $uneLigne->getQuantite()*$uneLigne->getMontantFrais();
            }
                      // ----- Ligne Frais Hors Forfait ----- //
            foreach ($ficheFrai->getLigneFraisHorsForfaits() as $uneLigneHF) {
                // Traitement justificatif
              $currentJustificatif = $formFiche['ligneFraisHorsForfaits'][$i]['justificatif']->getData();
              $i++;
              if ($currentJustificatif) {
                $nbJustificatif++;
                $originalFilename = pathinfo($currentJustificatif->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$currentJustificatif->guessExtension();
                $currentJustificatif->move($this->getParameter('justificatifs_directory'),$newFilename);
                $newJustificatif = new Justificatif();
                $newJustificatif->setFkUser(
                  $this->getDoctrine()->getRepository(Users::class)->find($this->session->get('id'))
                );
                $newJustificatif->setLigneFraisHorsForfait($uneLigneHF);
                $newJustificatif->setDateAjout(new \DateTime());
                $newJustificatif->setChemin($newFilename);
                $uneLigneHF->setFkJustificatif($newJustificatif);
              }
              $uneLigneHF->setDateMois($ficheFrai->getMois());
              $uneLigneHF->setFkFicheFrais($ficheFrai);
              $totalFF += $uneLigneHF->getMontant();
            }
            $ficheFrai->setMontantTotal($totalFF);
            $ficheFrai->setNbJustificatifs($nbJustificatif);
              // Fin des changements, enregistrement en BDD
            $entityManager->flush();
            return $this->redirectToRoute('fiche_frais_index');
          }

          return $this->render('fiche_frais/new.html.twig', [
          'fiche_frai' => $ficheFrai,
          'form' => $formFiche->createView(),
          'listeFraisForfait' => $arrayFraisForfait
          ]);
        } else {
          return $this->redirectToRoute('dashboard');
        }
    }

    /**
     * @Route("/{id}", name="fiche_frais_show", methods={"GET"})
     */
    public function show(FicheFrais $ficheFrai): Response
    {
        return $this->render('fiche_frais/show.html.twig', [
            'fiche_frai' => $ficheFrai,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="fiche_frais_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, FicheFrais $ficheFrai): Response
    {
        $arrayFraisForfait = $this->getDoctrine()->getRepository(FraisForfait::class)->findAll();
        $form = $this->createForm(FicheFraisType::class, $ficheFrai);
        $justificatifsChemin = [];

          // On récupère les lignes hors forfaits avant modifications pour assurer la persistence une fois qu'on submit
        $originalLHFs = new ArrayCollection();
        foreach ($ficheFrai->getLigneFraisHorsForfaits() as $originalLHF) {
            // Il semble que Symfony ne charge pas directement toutes les données intriquées lorsque l'on instancie un objet, à la place il effectue
            // un 'lazy loading'. Pour contourner ça, on effectue manuellement une requete pour qu'il aille chercher les données nécessaires.
            // Plus d'infos ici => https://symfony.com/doc/current/doctrine/associations.html#fetching-related-objects
          $justificatifsChemin[] = $originalLHF->getFkJustificatif()->getChemin();
          $originalLHFs->add($originalLHF);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
              // On vérifie si il y a des LHF a supprimées en comparant les LHF originels et celles en cours
            foreach ($originalLHFs as $LHF) {
              if ($ficheFrai->getLigneFraisHorsForfaits()->contains($LHF) === false) {
                $entityManager->remove($LHF);
              }
            }
              // Set les valeurs par defaut
            $ficheFrai->setDateModification(new \DateTime());
            $ficheFrai->setNbJustificatifs(sizeof($ficheFrai->getLigneFraisHorsForfaits()));
            $totalFF = 0; // Variables qui va récupérer les differents montants sur les lignes
            $totalValideFF = 0; // Pareil pour les frais validés
            $nbJustificatif = 0;
            $i = 0;
            foreach ($ficheFrai->getLigneFraisForfaits() as $uneLigne) {
              $uneLigne->setFkFicheFrais($ficheFrai);
              $totalFF += $uneLigne->getQuantite()*$uneLigne->getMontantFrais();
              if ($uneLigne->getEtatValidationLigne()) $totalValideFF += $uneLigne->getQuantite()*$uneLigne->getMontantFrais();
            }
            foreach ($ficheFrai->getLigneFraisHorsForfaits() as $uneLigneHF) {
                // Traitement justificatif
              if($this->session->get('role') === "Visiteur") {
                $currentJustificatif = $form['ligneFraisHorsForfaits'][$i]['justificatif']->getData();
                $i++;
                if ($currentJustificatif) {
                  $nbJustificatif++;
                  $originalFilename = pathinfo($currentJustificatif->getClientOriginalName(), PATHINFO_FILENAME);
                  $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                  $newFilename = $safeFilename.'-'.uniqid().'.'.$currentJustificatif->guessExtension();
                  $currentJustificatif->move($this->getParameter('justificatifs_directory'),$newFilename);
                  $newJustificatif = new Justificatif();
                  $newJustificatif->setFkUser(
                    $this->getDoctrine()->getRepository(Users::class)->find($this->session->get('id'))
                  );
                  $newJustificatif->setLigneFraisHorsForfait($uneLigneHF);
                  $newJustificatif->setDateAjout(new \DateTime());
                  $newJustificatif->setChemin($newFilename);
                  $uneLigneHF->setFkJustificatif($newJustificatif);
                }
              }
              $uneLigneHF->setDateMois($ficheFrai->getMois());
              $uneLigneHF->setFkFicheFrais($ficheFrai);
              $totalFF += $uneLigneHF->getMontant();
              if ($uneLigneHF->getValidee()) $totalValideFF += $uneLigneHF->getMontant();
            }
            $ficheFrai->setMontantTotal($totalFF);
            $ficheFrai->setMontantValide($totalValideFF);
              // Fin des changements, enregistrement en BDD
            $entityManager->flush();
            return $this->redirectToRoute('fiche_frais_index');
        }

        return $this->render('fiche_frais/edit.html.twig', [
            'fiche_frai' => $ficheFrai,
            'form' => $form->createView(),
            'listeFraisForfait' => $arrayFraisForfait,
            'justificatifsChemin' => $justificatifsChemin
        ]);
    }

    /**
     * @Route("/{id}", name="fiche_frais_delete", methods={"DELETE"})
     */
    public function delete(Request $request, FicheFrais $ficheFrai): Response
    {
        if ($this->isCsrfTokenValid('delete'.$ficheFrai->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($ficheFrai);
            $entityManager->flush();
        }
        return $this->redirectToRoute('fiche_frais_index');
    }
}
