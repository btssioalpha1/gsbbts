<?php

namespace App\Controller;

use App\Repository\FicheFraisRepository;
use App\Repository\LigneFraisForfaitRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class JavaModificationFicheController extends AbstractController
{
  /**
   * @Route("/api/ficheuser", name="java_affichage_fiche", methods={"GET"})
   */
  public function affichage(FicheFraisRepository $ficheFraisRepository, LigneFraisForfaitRepository $ligneFraisForfaitRepository)
  {
      $request = Request::createFromGlobals();

      $id = $request->query->get('id');
      $ficheFrai = $ficheFraisRepository->findLast($id);
      if ($ficheFrai !== null) {
        $ligneFrais = $ficheFrai->getLigneFraisForfaits();
        $arrayLigne = [];
        foreach ($ligneFrais as $key => $value) {
          $arrayLigne[] = $value;
        }
        return $this->json($arrayLigne, 200, [], ['groups'=>'post:read']);
      } else {
        return $this->json([], 200);
      }
  }

    /**
     * @Route("/api/modification/ficheuser", name="java_modification_fiche", methods={"PUT"})
     */
    public function envoiModif(LigneFraisForfaitRepository $ligneFraisForfaitRepository)
    {
        //Recupere la requete http
      $request = Request::createFromGlobals();
      $entityManager = $this->getDoctrine()->getManager();
        //Recupere les valeurs dans la requete http
      $nouvellesQuantite = explode(",",$request->request->get("nouvellesQuantite"));
      $ids = explode(",",$request->request->get("ids"));
      for ($i=0; $i < count($nouvellesQuantite); $i++) {
        $ligneAModif = $ligneFraisForfaitRepository->findOneById($ids[$i]);
        $ligneAModif->setQuantite($nouvellesQuantite[$i]);
        $entityManager->persist($ligneAModif);
      }
      $entityManager->flush();
      return $this->json("Changements effectues", 200);
    }
}
