<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\FicheFrais;
use App\Repository\FicheFraisRepository;
use Doctrine\ORM\EntityManagerInterface;
use Dompdf\Dompdf;
use Dompdf\Options;

$request = Request::createFromGlobals();


class StatistiquesController extends AbstractController
{
    /**
     * @Route("/statistiques", name="statistiques")
     */
    public function index(FicheFraisRepository $FicheF_repos, Request $request)
    {
      $statsFixe = $FicheF_repos->statFFparMois();
      //dd($statsFixe);
      return $this->render('statistiques/index.html.twig', [
        'statsFixe' => $statsFixe,
      ]);
    }

    /**
     * @Route("/traitement", name="traitement", methods={"POST"})
     */
    public function traitement(FicheFraisRepository $FicheF_repos, Request $request){
      $request = Request::createFromGlobals();
      $entityManager = $this->getDoctrine()->getManager();

      //On recupere les dates
      $dateDebut = "'".$request->request->get("dateDebut")."'";
      $dateFin = "'".$request->request->get("dateFin")."'";
      //dd($dateDebut." jusqu'a".$dateFin);

      //Création du queryBuilder en lien avec le repository voulu
      //Le QueryBuilder nous permettra de créer une instance afin de lancer notre requête DQL
      $res = $FicheF_repos->createQueryBuilder('a')
      //Ensuite on traduit notre requête SQL en DQL
      ->select("YEAR(a.mois) as year, DATE_FORMAT(a.mois, '%M') as month, SUM(a.montantTotal) as somme, COUNT(a.id) as nb, AVG(a.montantTotal) as moyenneMonth")
      ->where("a.mois BETWEEN ".$dateDebut." AND ".$dateFin)
      ->groupBy('year, month')
      //Ensuite ici on transforme l'instance du query builder en objet de requête
      ->getQuery()
      //On lui indique que le résultat retourné sera sous forme de tableau
      ->getArrayResult();
      //On initialise un élément réponse qui contiendra notre requete DQL en direction du Javascript
      $response = new Response(
          'Content',
          Response::HTTP_OK,
          ['content-type' => 'text/html']
      );

      $resfinal = json_encode($res);

      $response->setContent($resfinal);

      //Puis on demande d'envoyer le rendu sur la page statistiques
      return $response;
    }

    /**
     * @Route("/generate_pdf", name="generate_pdf")
     */
     public function generatePdf(FicheFraisRepository $FicheF_repos, Request $request){
       $statsFixe = $FicheF_repos->statFFparMois();
       // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('statistiques/index.html.twig', [
            'title' => "Welcome to our PDF Test",
            'statsFixe' => $statsFixe,
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("satts.pdf", [
            "Attachment" => true
        ]);
      }
}
