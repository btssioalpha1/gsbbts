<?php

namespace App\Controller;

use App\Entity\MaSession;
use App\Form\MaSessionType;
use App\Repository\MaSessionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/ma/session")
 */
class MaSessionController extends AbstractController
{
    /**
     * @Route("/", name="ma_session_index", methods={"GET"})
     */
    public function index(MaSessionRepository $maSessionRepository): Response
    {
        return $this->render('ma_session/index.html.twig', [
            'ma_sessions' => $maSessionRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="ma_session_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $maSession = new MaSession();
        $form = $this->createForm(MaSessionType::class, $maSession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($maSession);
            $entityManager->flush();

            return $this->redirectToRoute('ma_session_index');
        }

        return $this->render('ma_session/new.html.twig', [
            'ma_session' => $maSession,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ma_session_show", methods={"GET"})
     */
    public function show(MaSession $maSession): Response
    {
        return $this->render('ma_session/show.html.twig', [
            'ma_session' => $maSession,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="ma_session_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, MaSession $maSession): Response
    {
        $form = $this->createForm(MaSessionType::class, $maSession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ma_session_index');
        }

        return $this->render('ma_session/edit.html.twig', [
            'ma_session' => $maSession,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ma_session_delete", methods={"DELETE"})
     */
    public function delete(Request $request, MaSession $maSession): Response
    {
        if ($this->isCsrfTokenValid('delete'.$maSession->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($maSession);
            $entityManager->flush();
        }

        return $this->redirectToRoute('ma_session_index');
    }
}
