<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\MaSession;
use App\Repository\MaSessionRepository;
use App\Entity\Users;
use App\Repository\UsersRepository;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\HttpFoundation\Request;


class JavaConnexionController extends AbstractController
{
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
  {
    $this->passwordEncoder = $passwordEncoder;
  }
    /**
     * @Route("/java_connexion", name="java_connexion", methods={"POST"})
     */
    public function index(MaSessionRepository $ConnexionRepo, UsersRepository $UserRepo)
    {
        //Recupere la requete http
        $request = Request::createFromGlobals();
        $entityManager = $this->getDoctrine()->getManager();
        //On recupere le login et le mdp
        $loginJava = $request->request->get("login");
        $pwdJava = $request->request->get("password");


        $sessionJava = $ConnexionRepo->findOneBy(["login"=>$loginJava]);
        $hash = $sessionJava->getPassword();
        //Crypter le mdp qu'on récupère
        $verifPwd = password_verify($pwdJava, $hash);

        //Récupérer l'Id de la session pour trouver l'id User
        //afin de modifier les fiche frais coté mobile (voir Tristan)
        $newUserJava = new Users;
        $idSessionJava = $sessionJava->getId();
        $newUserJava = $UserRepo->findOneBy(["maSession"=>$idSessionJava]);
        $idJavaUser= $newUserJava->getId();

        //On met la condition suivante :
        //Si notre verification de mot de passe retourne TRUE alors,
        //on retournera l'Id de l'utilisateur qui se connecte vers l'application JAVA.
        if ($verifPwd === true){
          return $this->json($idJavaUser, 200);
        }
        //Sinon on retourne un code impossible, pour refuser l'accès.
        else {
          //retourner une variable "impossible" afin que dans la partie
          //JAVA on comprenne que les identifiants ne sont pas bons (ex: -10)
          $accessDenied = -1;
          return $this->json($accessDenied, 200);
        }
    }

    /**
     * @Route("/api/profil", name="java_affichage_profil", methods={"GET"})
     */
    public function affichage(UsersRepository $UserRepo)
    {
        $request = Request::createFromGlobals();

        $id = $request->query->get('id');
        $userJAVA = $UserRepo->findOneById($id);
        $infos = [];
        if ($userJAVA !== null) {
          $infos["nom"] = $userJAVA->getNom();
          $infos["prenom"] = $userJAVA->getPrenom();
          $infos["num_tel"] = $userJAVA->getTelephone();
          $infos["adresse"] = $userJAVA->getAdresse();
        }
        return $this->json($infos, 200);
    }

}
