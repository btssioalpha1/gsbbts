<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LigneFraisHorsForfaitRepository")
 */
class LigneFraisHorsForfait
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateMois;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $libelle;


    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\Column(type="float")
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FicheFrais", inversedBy="ligneFraisHorsForfaits")
     */
    private $fk_FicheFrais;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $validee;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Justificatif", inversedBy="ligneFraisHorsForfait", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $fkJustificatif;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateMois(): ?\DateTimeInterface
    {
        return $this->dateMois;
    }

    public function setDateMois(\DateTimeInterface $dateMois): self
    {
        $this->dateMois = $dateMois;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getMontant(): ?float
    {
        return $this->montant;
    }

    public function setMontant(float $montant): self
    {
        $this->montant = $montant;

        return $this;
    }

    public function getFkFicheFrais(): ?FicheFrais
    {
        return $this->fk_FicheFrais;
    }

    public function setFkFicheFrais(?FicheFrais $fk_FicheFrais): self
    {
        $this->fk_FicheFrais = $fk_FicheFrais;

        return $this;
    }

    public function getValidee(): ?bool
    {
        return $this->validee;
    }

    public function setValidee(?bool $validee): self
    {
        $this->validee = $validee;

        return $this;
    }

    public function getFkJustificatif(): ?Justificatif
    {
        return $this->fkJustificatif;
    }

    public function setFkJustificatif(Justificatif $fkJustificatif): self
    {
        $this->fkJustificatif = $fkJustificatif;

        return $this;
    }
}
