<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LigneFraisForfaitRepository")
 */
class LigneFraisForfait
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("post:read")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateMois;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FraisForfait", inversedBy="ligneFraisForfaits")
     * @Groups("post:read")
     */
    private $fk_FraisForfait;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FicheFrais", inversedBy="ligneFraisForfaits")
     */
    private $fk_FicheFrais;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $etatValidationLigne;

    public function __construct()
    {

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDateMois(): ?\DateTimeInterface
    {
        return $this->dateMois;
    }

    public function setDateMois(\DateTimeInterface $dateMois): self
    {
        $this->dateMois = $dateMois;

        return $this;
    }

    public function getQuantite(): ?int
    {
        return $this->quantite;
    }

    public function setQuantite(int $quantite): self
    {
        $this->quantite = $quantite;

        return $this;
    }

    public function getFkFraisForfait(): ?FraisForfait
    {
        return $this->fk_FraisForfait;
    }

    public function setFkFraisForfait(?FraisForfait $fk_FraisForfait): self
    {
        $this->fk_FraisForfait = $fk_FraisForfait;

        return $this;
    }

    public function getFkFicheFrais(): ?FicheFrais
    {
        return $this->fk_FicheFrais;
    }

    public function setFkFicheFrais(?FicheFrais $fk_FicheFrais): self
    {
        $this->fk_FicheFrais = $fk_FicheFrais;

        return $this;
    }

    public function __toString() {
      return strval($this->getQuantite());
    }

    public function getEtatValidationLigne(): ?bool
    {
        return $this->etatValidationLigne;
    }

    public function setEtatValidationLigne(string $etatValidationLigne): self
    {
        $this->etatValidationLigne = $etatValidationLigne;

        return $this;
    }

    public function getMontantFrais(): float
    {
      return $this->fk_FraisForfait->getMontant();
    }
}
