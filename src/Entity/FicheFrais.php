<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FicheFraisRepository")
 */
class FicheFrais
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $mois;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbJustificatifs;

    /**
     * @ORM\Column(type="float")
     */
    private $montantValide;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateModification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="ficheFrais")
     */
    private $fk_users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LigneFraisForfait", mappedBy="fk_FicheFrais", cascade={"persist"})
     */
    private $ligneFraisForfaits;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LigneFraisHorsForfait", mappedBy="fk_FicheFrais", cascade={"persist"})
     */
    private $ligneFraisHorsForfaits;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etat", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $fk_Etat;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Vehicule", inversedBy="ficheFrais")
     */
    private $fk_vehicule;

    /**
     * @ORM\Column(type="float")
     */
    private $montantTotal;

    public function __construct()
    {
        $this->ligneFraisForfaits = new ArrayCollection();
        $this->ligneFraisHorsForfaits = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMois(): ?\DateTimeInterface
    {
        return $this->mois;
    }

    public function setMois(\DateTimeInterface $mois): self
    {
        $this->mois = $mois;

        return $this;
    }

    public function getNbJustificatifs(): ?int
    {
        return $this->nbJustificatifs;
    }

    public function setNbJustificatifs(int $nbJustificatifs): self
    {
        $this->nbJustificatifs = $nbJustificatifs;

        return $this;
    }

    public function getMontantValide(): ?float
    {
        return $this->montantValide;
    }

    public function setMontantValide(float $montantValide): self
    {
        $this->montantValide = $montantValide;

        return $this;
    }

    public function getDateModification(): ?\DateTimeInterface
    {
        return $this->dateModification;
    }

    public function setDateModification(\DateTimeInterface $dateModification): self
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    public function getFkUsers(): ?Users
    {
        return $this->fk_users;
    }

    public function setFkUsers(?Users $fk_users): self
    {
        $this->fk_users = $fk_users;

        return $this;
    }

    /**
     * @return Collection|LigneFraisForfait[]
     */
    public function getLigneFraisForfaits(): Collection
    {
        return $this->ligneFraisForfaits;
    }

    public function addLigneFraisForfait(LigneFraisForfait $ligneFraisForfait): self
    {
        if (!$this->ligneFraisForfaits->contains($ligneFraisForfait)) {
            $this->ligneFraisForfaits[] = $ligneFraisForfait;
            $ligneFraisForfait->setFkFicheFrais($this);
        }

        return $this;
    }

    public function removeLigneFraisForfait(LigneFraisForfait $ligneFraisForfait): self
    {
        if ($this->ligneFraisForfaits->contains($ligneFraisForfait)) {
            $this->ligneFraisForfaits->removeElement($ligneFraisForfait);
            // set the owning side to null (unless already changed)
            if ($ligneFraisForfait->getFkFicheFrais() === $this) {
                $ligneFraisForfait->setFkFicheFrais(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LigneFraisHorsForfait[]
     */
    public function getLigneFraisHorsForfaits(): Collection
    {
        return $this->ligneFraisHorsForfaits;
    }

    public function addLigneFraisHorsForfait(LigneFraisHorsForfait $ligneFraisHorsForfait): self
    {
        if (!$this->ligneFraisHorsForfaits->contains($ligneFraisHorsForfait)) {
            $this->ligneFraisHorsForfaits[] = $ligneFraisHorsForfait;
            $ligneFraisHorsForfait->setFkFicheFrais($this);
        }

        return $this;
    }

    public function removeLigneFraisHorsForfait(LigneFraisHorsForfait $ligneFraisHorsForfait): self
    {
        if ($this->ligneFraisHorsForfaits->contains($ligneFraisHorsForfait)) {
            $this->ligneFraisHorsForfaits->removeElement($ligneFraisHorsForfait);
            // set the owning side to null (unless already changed)
            if ($ligneFraisHorsForfait->getFkFicheFrais() === $this) {
                $ligneFraisHorsForfait->setFkFicheFrais(null);
            }
        }

        return $this;
    }

    public function getFkEtat(): ?Etat
    {
        return $this->fk_Etat;
    }

    public function setFkEtat(?Etat $fk_Etat): self
    {
        $this->fk_Etat = $fk_Etat;

        return $this;
    }

    public function getFkVehicule(): ?Vehicule
    {
        return $this->fk_vehicule;
    }

    public function setFkVehicule(?Vehicule $fk_vehicule): self
    {
        $this->fk_vehicule = $fk_vehicule;

        return $this;
    }

    public function __toString() {
      return 'test';
    }

    public function getMontantTotal(): ?float
    {
        return $this->montantTotal;
    }

    public function setMontantTotal(?float $montantTotal): self
    {
        $this->montantTotal = $montantTotal;

        return $this;
    }
}
