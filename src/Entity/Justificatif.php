<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\JustificatifRepository")
 */
class Justificatif
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $chemin;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateAjout;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Users", inversedBy="justificatifs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fkUser;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\LigneFraisHorsForfait", mappedBy="fkJustificatif", cascade={"persist", "remove"})
     */
    private $ligneFraisHorsForfait;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChemin(): ?string
    {
        return $this->chemin;
    }

    public function setChemin(string $chemin): self
    {
        $this->chemin = $chemin;

        return $this;
    }

    public function getDateAjout(): ?\DateTimeInterface
    {
        return $this->dateAjout;
    }

    public function setDateAjout(\DateTimeInterface $dateAjout): self
    {
        $this->dateAjout = $dateAjout;

        return $this;
    }

    public function getFkUser(): ?Users
    {
        return $this->fkUser;
    }

    public function setFkUser(?Users $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    public function getLigneFraisHorsForfait(): ?LigneFraisHorsForfait
    {
        return $this->ligneFraisHorsForfait;
    }

    public function setLigneFraisHorsForfait(LigneFraisHorsForfait $ligneFraisHorsForfait): self
    {
        $this->ligneFraisHorsForfait = $ligneFraisHorsForfait;

        // set the owning side of the relation if necessary
        if ($ligneFraisHorsForfait->getFkJustificatif() !== $this) {
            $ligneFraisHorsForfait->setFkJustificatif($this);
        }

        return $this;
    }
}
