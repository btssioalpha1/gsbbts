// Attendre que la page soit prête pour commencer

$(document).ready(function(){
  /*
  ici c'est pour le cas où on modifie la date et où on doit donc récupérer
  les valeurs déjà présentent pour initaliser les min et max selon ce qui est déjà renseigné
  */

  // Récupération des valeurs des input
  let dateDebut = $('#dateDebut').val();

  let dateFin = $('#dateFin').val();

  // Si dateDebut n'est pas vide, on affecte la valeur min à dateFin
  if(dateDebut != "")
   $('#dateFin').attr('min', dateDebut);

  // Si dateFin n'est pas vide, on affecte la valeur max à dateDebut
  if(dateFin != "")
   $('#dateDebut').attr('max', dateFin);

  /* Maintenant le code pour quand il y ai une action sur un des deux input */
  // Au changement sur l'input date "dateDebut"
  $('#dateDebut').change(function()
  {
     // Récupération de la valeur de l'input
     let dateDebut = $('#dateDebut').val();

     // Affectation de la valeur à l'attribut "min" de l'input "dateFin"
     $('#dateFin').attr('min', dateDebut);
  })

  // Au changement sur l'input date "dateFin"
  $('#dateFin').change(function()
  {
     // Récupération de la valeur de l'input
     let dateFin = $('#dateFin').val();

     // Affectation de la valeur à l'attribut "max" de l'input "dateDebut"
     $('#dateDebut').attr('max', dateFin);
  })
});
