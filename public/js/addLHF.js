
let collectionHolder = document.getElementById('js-container-LHF'); // ul qui contient le prototype de form des LHF
let addButton = document.getElementById('js-add-button');
let removeButton = document.getElementById('js-remove-button');
let newIndex = collectionHolder.querySelectorAll("li").length;
// On se base sur le nombre de li dans la liste des LHF pour avoir le bon indice en cas d'edit

if(addButton) {
    // Ajout
  addButton.addEventListener('click', function(){
      // Récupère le prototype du form
    let prototype = collectionHolder.getAttribute('data-prototype');
    let newForm = prototype;
      // On insère le bon index
    newForm = newForm.replace(/__name__/g, newIndex);
    newIndex++;
    newForm = `<div style="margin: 30px;"><b>LHF n°${newIndex} :</b></div>` + newForm; // Titre
      // On ajoute le form a une li venant d'être créé puis on l'ajoute à l'ul
    let newFormLi = document.createElement('li');
    newFormLi.id = `js-LHF-${newIndex}`;
    newFormLi.innerHTML = newForm;
    collectionHolder.appendChild(newFormLi);

      // Ajout de l'event pour ajouter le nom du fichier dans l'input
    let justificatifInput = document.getElementById(`fiche_frais_ligneFraisHorsForfaits_${newIndex-1}_justificatif`); // (-1 : newIndex est déjà incrémenté)
    if (justificatifInput) {
      justificatifInput.addEventListener('change', function() {
        let fakePathLess = this.value.slice(this.value.lastIndexOf('\\') + 1);
        this.nextSibling.innerText = fakePathLess;
        this.nextSibling.setAttribute('style','color: black');
      })
    }
  });
}

if(removeButton) {
    // Suppresion
  removeButton.addEventListener('click', function(){
    if (newIndex > 0) {
      console.log('Suppression de js-LHF-'+newIndex);
      let formToRemove = document.getElementById('js-LHF-'+newIndex);
      newIndex--;
      formToRemove.parentNode.removeChild(formToRemove);
    }
  });
}
