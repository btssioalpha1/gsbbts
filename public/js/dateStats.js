$(function(){
  $('#datepicker').click(function(){

    // On récupère la zone d'affichage des graphes,
    //on l'initialise à sa valeur par défaut pour éviter la superposition des graphes
    $('#graphStats').html('<canvas id="graph-montant-nbNote" width="100%"></canvas>');
    $('#graphStats2').html('<canvas id="graph-moyenne-mois" width="100%"></canvas>');

    // Lors du click sur le bouton, on récupère et on initialise les variables
    var dateD = $('#dateDebut').val();
    var dateF = $('#dateFin').val();

    // Si les champs ne sont pas vides alors
    if ((dateD !== "") && (dateF !== "")) {
      //récupération des données dans un objet json
      var stats = {
      'dateDebut': $('#dateDebut').val(),
      'dateFin': $('#dateFin').val()
      };

      // envoi de la requete vers Symfony
      $.ajax({
        url : '/traitement', // L'url symfony (la route)
        data: stats, // Les données à transférer
        method: 'POST', // La méthode de requête utilisée
        dataType: 'text', // le type de données qui seront retournées par Symfony
        success: function (data) {
          //traitement sur les données de retour une fois que symfony à renvoyé un code 200 ou 201 (une réussite de la requete)
          var retour = data;
          var tableStatsRetour = JSON.parse(retour);

          //initialisation variables
          var tableStats = $('#tbodyStats');
          var montantTotal = 0;
          var moyenneTotale = 0;

          //On vide le tableau pour le remplir avec les nouvelles données en dynamique
          $(tableStats).empty();
          $.each(tableStatsRetour, function(key, value){
            let newFormTr = document.createElement('tr');
            var newTr =
            `<td>${value.year} </td>` +
            `<td>${value.month} </td>` +
            `<td>${value.somme} </td>` +
            `<td>${value.nb} </td>` +
            `<td>${value.moyenneMonth} </td>`
            ;
            //Calcul du montant total de toutes les fiches frais
            montantTotal += Number(value.somme);

            newFormTr.innerHTML = newTr;
            tableStats.append(newFormTr);
          })
          //Calcul de la moyenne du total des fiches frais
          moyenneTotale = montantTotal/tableStatsRetour.length;



          //On fais apparaitre tous les éléments HTML
          $('#montantTotal').html("Montant total des fiches de frais : <span id='js-montant-total'></span>");
          $('#js-montant-total').html(montantTotal.toFixed(2) + " €");

          $('#moyenneFF').html("Moyenne des montants totaux des fiches de frais : <span id='js-moyenne-ff'>");
          $('#js-moyenne-ff').html(moyenneTotale.toFixed(2) + " €");

          $('#titleGraphe1').html("Graphique du nombre de fiches par mois");
          $('#titleGraph2').html("Graphique des montants moyens par mois");


          //On initialise les variables
          var aListeMois = [];
          var aDataNbFiches = [];
          var aDataMoyenne = [];

          //On remplis les différents tableau de valeurs utilisés dans les graphes
          $.each(tableStatsRetour, function(key, value){
            aListeMois.push(value.month + " " + value.year);
            aDataNbFiches.push(value.nb);
            aDataMoyenne.push(value.moyenneMonth);
          });


          //Graphe 1
          var ctxMoyenneNbNotes = document.getElementById('graph-montant-nbNote');
          var aDataGraphe1 = {
            "label": "Nombre de notes par mois",
            "libelleColonne": aListeMois,
            "data": aDataNbFiches
          };
          graphBar(ctxMoyenneNbNotes, aDataGraphe1);


          //Graphe 2
          var ctxMoyenneMois = document.getElementById('graph-moyenne-mois');
          var aDataGraphe2 = {
            "label": "Moyenne des fiches par mois",
            "libelleColonne": aListeMois,
            "data": aDataMoyenne
          };
          graphBar(ctxMoyenneMois, aDataGraphe2);
        },

        //Si ça ne fonctionne pas émettre un message pour l'utilisateur
        error: function () {
          //traitement en cas d'erreur
          alert('Erreur veuillez contacter le support.');
        }
      });
    }
    //Si les dates ne sont pas remplis afficher msg
    else {
      alert('veuillez sélectionner un intervalle de date valide svp.');
    }
  });
})

//Fonction permettant la génération des grpahes (chartJS)
function graphBar(ctx, data){

  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: data.libelleColonne,
          datasets: [{
              label: data.label,
              data: data.data,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              borderColor: [
                  'rgba(255, 99, 132, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(255, 206, 86, 1)',
                  'rgba(75, 192, 192, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(255, 159, 64, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
}
