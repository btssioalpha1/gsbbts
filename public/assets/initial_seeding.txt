INSERT INTO `etat` (`id`, `libelle`) VALUES (NULL, 'Saisie'), (NULL, 'En traitement');
INSERT INTO `etat` (`id`, `libelle`) VALUES (NULL, 'Traité'), (NULL, 'Payé');
INSERT INTO `frais_forfait` (`id`, `libelle`, `montant`) VALUES (NULL, 'Restaurant', '30'), (NULL, 'Hotel', '60');
INSERT INTO `frais_forfait` (`id`, `libelle`, `montant`) VALUES (NULL, 'Kilometrage', '0.75');
INSERT INTO `ma_session` (`id`, `login`, `password`) VALUES (NULL, 'gsb', '$2y$13$bhSDzB1GE1t3j7tvvRTe3uYQgVXYckWETbm8nthnqFsMHvLBpFqua');
INSERT INTO `ma_session` (`id`, `login`, `password`) VALUES (NULL, 'gsbComptable', '$2y$13$bhSDzB1GE1t3j7tvvRTe3uYQgVXYckWETbm8nthnqFsMHvLBpFqua');
INSERT INTO `ma_session` (`id`, `login`, `password`) VALUES (NULL, 'gsbVisiteur', '$2y$13$bhSDzB1GE1t3j7tvvRTe3uYQgVXYckWETbm8nthnqFsMHvLBpFqua');
INSERT INTO `role` (`id`, `libelle`) VALUES (NULL, 'Administrateur');
INSERT INTO `role` (`id`, `libelle`) VALUES (NULL, 'Comptable'), (NULL, 'Visiteur');
INSERT INTO `vehicule` (`id`, `cv_fiscaux`) VALUES (NULL, '3'), (NULL, '4'), (NULL, '5'), (NULL, '6');
INSERT INTO `users` (`id`, `ma_session_id`, `role_id`, `nom`, `prenom`, `date_naissance`, `code_postal`, `ville`, `adresse`, `telephone`, `matricule`, `email`, `date_embauche`) VALUES (NULL, '1', '1', 'Tristan', 'Le Bo Gosse', '1994-09-26 00:00:00', '42300', 'Singapour', 'La grosse villa du bord de plage', 'Un Samsung Galaxy S7 tmtc', 'El Padre Dell Informatico', 'izuku@peteLeStyle.anime', '2020-02-01 00:00:00');
INSERT INTO `users` (`id`, `ma_session_id`, `role_id`, `nom`, `prenom`, `date_naissance`, `code_postal`, `ville`, `adresse`, `telephone`, `matricule`, `email`, `date_embauche`) VALUES (NULL, '2', '2', 'Semih', 'Le GROS Bo Gosse un peu malade', '1994-09-26 00:00:00', '42300', 'Bogota', '93 les pyramides', 'Un iphone avec poke', '4242', 'izuku@peteLeStyle.anime', '2020-02-01 00:00:00');
INSERT INTO `users` (`id`, `ma_session_id`, `role_id`, `nom`, `prenom`, `date_naissance`, `code_postal`, `ville`, `adresse`, `telephone`, `matricule`, `email`, `date_embauche`) VALUES (NULL, '3', '3', 'Alexis', 'Chomeur de profession', '1994-09-26 00:00:00', '42300', 'Lyon', 'Boite de nuit', 'heellllooooo', '00000', 'izuku@peteLeStyle.anime', '2020-02-01 00:00:00');
